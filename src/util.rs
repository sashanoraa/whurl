use gtk::gio::{File, FileQueryInfoFlags};
use gtk::prelude::*;

pub struct ResouceInfo {
	pub resource: String,
	pub scheme: Option<String>,
	pub content_type: String,
}

pub fn read_resource(file: &File) -> ResouceInfo {
	const DEFAULT_CONENT_TYPE: &str = "application/octet-stream";
	// TODO handle xdg portal nonsense
	let resource = file.parse_name().into();
	let scheme: Option<String> = file.uri_scheme().map(|s| s.into());
	if let Some(scheme) = &scheme {
		if scheme != "file" {
			let content_type = format!("x-scheme-handler/{scheme}");
			return ResouceInfo {
				resource,
				scheme: Some(scheme.to_owned()),
				content_type,
			};
		}
	}
	match file.query_info(
		"standard::content-type",
		FileQueryInfoFlags::NONE,
		None::<&gtk::gio::Cancellable>,
	) {
		Ok(file_info) => ResouceInfo {
			resource,
			scheme,
			content_type: file_info
				.content_type()
				.map(|s| s.into())
				.unwrap_or_else(|| DEFAULT_CONENT_TYPE.to_owned()),
		},
		Err(e) => {
			eprintln!("Error geting file content type: {e:#}");
			ResouceInfo {
				resource,
				scheme,
				content_type: DEFAULT_CONENT_TYPE.to_owned(),
			}
		}
	}
}

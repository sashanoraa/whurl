use std::cell::RefCell;
use std::rc::Rc;

use adw::{Application, ApplicationWindow, ToolbarView};
use adw::{HeaderBar, WindowTitle};
use gtk::gio::{AppInfo, ApplicationFlags, File};
use gtk::glib::clone;
use gtk::{glib, Orientation};
use gtk::{prelude::*, Label, ListBox};
use gtk::{Box, Image};
use util::{read_resource, ResouceInfo};

mod util;

const APP_ID: &str = "gay.noraa.whurl";
const EXCLUDED_APPS: [&str; 4] = [
	APP_ID,
	"re.sonny.Junction.desktop",
	"com.properlypurple.braus.desktop",
	"spacefm.desktop",
];

#[async_std::main]
async fn main() -> glib::ExitCode {
	// Create a new application
	let app = Application::builder()
		.application_id(APP_ID)
		.flags(ApplicationFlags::HANDLES_OPEN)
		.build();

	if ashpd::is_sandboxed().await {
		if let Err(e) =
			glib::spawn_command_line_async("gio mime x-scheme-handler/https gay.noraa.whurl")
		{
			eprintln!("Error setting sandbox https handler by running `gio mime x-scheme-handler/https gay.noraa.whurl`: {e:#}");
		}
		if let Err(e) =
			glib::spawn_command_line_async("gio mime x-scheme-handler/http gay.noraa.whurl")
		{
			eprintln!("Error setting sandbox http handler by running `gio mime x-scheme-handler/https gay.noraa.whurl`: {e:#}");
		}
	}

	app.connect_open(|self_, files, _| {
		for file in files {
			build_ui(self_, Some(file));
		}
	});

	// Connect to "activate" signal of `app`
	app.connect_activate(|self_| build_ui(self_, None));

	// Run the application
	app.run()
}

struct AppLaunchInfo {
	file: File,
	apps: Vec<AppInfo>,
	this_app: Application,
}

fn build_ui(this_app: &Application, file: Option<&File>) {
	let app_list_box = ListBox::new();
	if let Some(file) = file {
		let app_launch_info = Rc::new(RefCell::new(AppLaunchInfo {
			file: (*file).clone(),
			apps: vec![],
			this_app: (*this_app).clone(),
		}));
		app_list_box.connect_row_activated(clone!(@strong app_launch_info => move |_, row| {
			println!("{}", row.index());
			let app_launch_info = app_launch_info.borrow();
			let app: &AppInfo = &app_launch_info.apps[row.index() as usize];
			if let Err(e) = app.launch(&[app_launch_info.file.clone()], None::<&gtk::gio::AppLaunchContext>) {
				eprintln!("Error launching app: {e:#}");
			}
			app_launch_info.this_app.quit();
		}));
		let ResouceInfo {
			resource,
			scheme,
			content_type,
		} = read_resource(file);
		println!("Resource: {resource}");
		if let Some(scheme) = &scheme {
			println!("Scheme: {scheme}");
		}
		println!("Content type: {content_type}");
		let apps = get_apps(&content_type);
		for app in &apps {
			println!("{}", app.display_name());
			let item = Box::builder()
				.orientation(Orientation::Horizontal)
				.halign(gtk::Align::Fill)
				.build();
			if let Some(icon) = &app.icon() {
				let icon = Image::from_gicon(icon);
				// icon.set_pixel_size(64);
				icon.set_icon_size(gtk::IconSize::Large);
				item.append(&icon);
			}
			let label = Label::new(Some(&app.display_name().to_string()));
			item.append(&label);
			app_list_box.append(&item);
		}
		app_launch_info.borrow_mut().apps = apps;
		// TODO do stuff
	}
	let win_title = WindowTitle::builder()
		.title("Open with")
		.focusable(true)
		.focus_on_click(true)
		.build();
	let header = HeaderBar::builder().title_widget(&win_title).build();
	// let content = Box::builder()
	// 	.orientation(Orientation::Horizontal)
	// 	.halign(gtk::Align::End)
	// 	.build();
	// content.append(&app_list_box);
	let main_view = ToolbarView::builder().content(&app_list_box).build();
	main_view.add_top_bar(&header);
	// Create a window and set the title
	let window = ApplicationWindow::builder()
		.application(this_app)
		.title("Whurl")
		.content(&main_view)
		.build();

	// Present window
	window.present();
}

fn get_apps(content_type: &str) -> Vec<AppInfo> {
	AppInfo::recommended_for_type(content_type)
		.into_iter()
		.filter(|app_info| !EXCLUDED_APPS.contains(&app_info.to_string().as_str()))
		.collect()
}

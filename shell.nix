{ pkgs ? import <nixpkgs> { } }:
with pkgs;
mkShell {
  buildInputs = [
    # rust deps
    cargo
    rustc
	 	rust-analyzer
		rustfmt

    # build deps
    pkg-config

    # gtk deps
    gtk4
    gdk-pixbuf
    cairo
    libadwaita
  ];
}
